Title: DebConf18 welcomes its first 16 sponsors!
Slug: dc18-welcome-its-first-sponsors
Date: 2018-03-10 15:15
Author: Laura Arjona Reina
Tags: debconf18, debconf, sponsors
Status: draft

![DebConf18 logo](|filename|/images/DebConf18_Horizontal_Logo_600_240.png)

DebConf18 will take place in Hsinchu, Taiwan, from July 29 to August 5, 2018. 
It will be the first Debian Annual Conference in Asia, and we anticipate 
600 attendees and major advances for Debian and for Free Software in general.
We extend an invitation to everyone to join us and to support this event. 
As a volunteer-run non-profit conference, we depend on our sponsors.

Sixteen companies have already committed to sponsor DebConf18! With a warm
welcome, we'd like to introduce them to you.

Our first Platinum sponsor is  [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource).
HPE is an industry-leading technology company
providing a comprehensive portfolio of products such as 
integrated systems, servers, storage, networking and software.
Their offer consulting, operational support and financial services,
and complete solutions for many different industries: mobile and IoT, 
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian, 
and provides hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

Our first Gold sponsor is [**Google**](http://google.com), the technology company 
specialized in Internet-related services as online advertising and search engine.

As Silver sponsors we have 
[**credativ**](http://www.credativ.de/) 
(a service-oriented company focusing on open-source software and also a 
[Debian development partner](https://www.debian.org/partners/)), 
[**Gandi**](https://www.gandi.net/),
FIXME-GANDI-DESCRIPTION,
[**Skymizer**](https://skymizer.com/)
FIXME-SKYMIZER-DESCRIPTION,
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
FIXME-CIP-DESCRIPTION,
[**Brandorr Group**](https://www.brandorr.com/),
FIXME-BRANDORR-DESCRIPTION,
[**3CX**](https://www.3cx.com/),
FIXME-3CX-DESCRIPTION,
[**Free Software Initiative Japan**](https://www.fsij.org/),
FIXME-FSIJ-DESCRIPTION,
[**Texas Instruments**](http://www.ti.com/) (the global semiconductor company),
and the [**Bern University of Applied Sciences**](https://www.bfh.ch/) 
(with over [6,800](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled, 
located in the Swiss capital).

[**ISG.EE**](http://isg.ee.ethz.ch/) and 
[**Univention**](https://www.univention.com/)
are our Bronze sponsors so far.

And finally, [**SLAT (Software Liberty Association of Taiwan)**](https://slat.org/),
[**The Linux foundation**](https://www.linuxfoundation.org/)
and [**deepin**](https://www.deepin.com/)
are our supporter sponsors.

## Become a sponsor too!

Would you like to become a sponsor? Do you know of or work in a company or
organization that may consider sponsorship?

Please have a look at our 
[sponsorship brochure](https://media.debconf.org/dc18/fundraising/debconf18_sponsorship_brochure_en.pdf)
(or a summarized [flyer](https://media.debconf.org/dc18/fundraising/debconf18_sponsorship_flyer_en.pdf)),
in which we outline all the details and describe the sponsor benefits. 

For further details, feel free to contact us through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf18 website at [https://debconf18.debconf.org](https://debconf18.debconf.org).
