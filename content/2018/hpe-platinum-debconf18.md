Title: Hewlett Packard Enterprise Platinum Sponsor of DebConf18
Slug: hpe-platinum-debconf18
Date: 2017-03-15 09:15
Author: Laura Arjona Reina
Tags: debconf18, debconf, sponsors, HPE
Status: draft

[![HPElogo](|filename|/images/hpe.png)](http://www.hpe.com/engage/opensource)

We are very pleased to announce that [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource) 
has committed support to [DebConf18](https://debconf18.debconf.org) as a **Platinum sponsor**.

*"Hewlett Packard Enterprise is excited to support Debian's annual developer 
conference again this year"*, said Steve Geary, Senior Director R&D at 
Hewlett Packard Enterprise. *"As Platinum sponsors and member of the Debian community, 
HPE is committed to supporting Debconf. The conference, community and  open 
distribution are foundational to the development of The Machine research 
program  and will our bring our Memory Driven Computing agenda to life."*

HPE is an industry-leading technology company
providing a comprehensive portfolio of products such as 
integrated systems, servers, storage, networking and software.
Their offer consulting, operational support and financial services,
and complete solutions for many different industries: mobile and IoT, 
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian, 
and provides hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

With this additional commitment as Platinum Sponsor, 
HPE contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software 
helping to strengthen the community that continues to collaborate on 
Debian projects throughout the rest of the year.

Thank you very much Hewlett Packard Enterprise, for your support of DebConf18!

## Become a sponsor too!

DebConf18 is still accepting sponsors. 
Interested companies and organizations may contact the DebConf team 
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf18 website at [https://debconf18.debconf.org](https://debconf18.debconf.org).
