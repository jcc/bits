Title: Help empower the Debian Outreach Program for Women
Date: 2014-10-16 19:30
Tags: opw
Slug: debian-opw-2014
Author: Tom Marble
Status: published

Debian is thrilled to participate in the 9th round of
the [GNOME FOSS Outreach Program](http://gnome.org/opw/).
While OPW is similar to
[Google Summer of Code](https://www.google-melange.com/) it has
a winter session in addition to a summer session and is
open to non-students.

Back at [DebConf 14](http://debconf14.debconf.org/) several of us
decided to volunteer because we want to increase diversity in Debian.
Shortly thereafter the [DPL](https://www.debian.org/devel/leader) announced
[Debian's participation in OPW 2014](https://lists.debian.org/debian-project/2014/08/msg00078.html).

We have reached out to several corporate sponsors and are thrilled
that so far [Intel](http://http://www.intel.com) has agreed to fund an intern slot (in addition to the slot
offered by the DPL)! While that makes two funded slots we have
a third sponsor that has offered a challenge match: for each dollar donated
by an individual to Debian the sponsor will donate another dollar for
Debian OPW.

This is where we need your help! If we can raise $3,125 by October 22
that means we can mentor a third intern ($6,250). Please
spread the word and donate today if you can at: [http://debian.ch/opw2014/](http://debian.ch/opw2014/)

If you'd like to participate as intern, the application deadline is
the same (October 22nd). You can find out more on the [Debian Wiki](http://wiki.debian.org/OutreachProgramForWomen).
