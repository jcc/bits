Title: Bầu cử lãnh đạo dự án Debian 2016, chúc mừng Mehdi Dogguy!
Date: 2016-04-17 18:40
Tags: dpl
Slug: results-dpl-elections-2016
Author: Ana Guerrero Lopez
Translator: Trần Xuân Giáp
Lang: vi
Status: published

Bầu cử lãnh đạo dự án Debian đã kết thúc vào ngày hôm qua và người thằng cuộc là Mehdi Dogguy!
Trong tổng số 1023 nhà phát triển, có 282 người đã bỏ phiếu bằng [phương pháp bầu cử](http://en.wikipedia.org/wiki/Condorcet_method).

Các thông tin về kết quả có tại [bầu cử người lãnh đạo dự án Debian 2016](https://www.debian.org/vote/2016/vote_001).

Nhiệm kỳ mới cho lãnh đạo dự án bắt đầu hôm nay, ngày 17 tháng 4 và kết thúc vào ngày 17 tháng 4 năm 2017.
