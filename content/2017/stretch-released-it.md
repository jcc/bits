Title: Rilasciato Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: it
Translator: Vincenzo Campanella

![È stato rilasciato Alt Stretch!](|filename|/images/banner_stretch.png)

Lasciatevi abbracciare da Stretch, il gioco del polpo purpureo. Siamo lieti di annunciare
il rilascio di Debian 9.0, dal nome in codice *Stretch*.

**Volete installarlo?**
Scegliete il vostro [supporto d'installazione](https://www.debian.org/distrib/) preferito fra Blu-ray, DVD, CD 
e stick USB, dopodiché leggete il [manuale d'installazione](https://www.debian.org/releases/stretch/installmanual).

**Siete già un felice utente Debian e desiderate solo effettuare l'aggiornamento?**
Per effettuare un facile aggiornamento dalla versione attuale, Debian 8 "Jessie", leggete le
[note di rilascio](https://www.debian.org/releases/stretch/releasenotes).

**Volete festeggiare il rilascio?**
Condividete il [banner da questo blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) nel vostro blog o nel vostro sito web!
